import { ProductModule } from './Product/Product.module';
import { ProductListComponent } from './Product/ProductList/ProductList.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'products',
    loadChildren: () => import('./Product/Product.module').then(mod => mod.ProductModule)
  },
  {
    path: 'movies',
    loadChildren: () => import('./Movies/movies.module').then(mod => mod.MoviesModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
