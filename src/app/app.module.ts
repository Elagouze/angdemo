import { MoviesModule } from './Movies/movies.module';
import { ProductModule } from './Product/Product.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './AppComponent';
import { NavComponent } from './Shared/Nav/Nav.component';

// Third Party Packages
import {SlimLoadingBarModule} from 'ng2-slim-loading-bar';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { SideBarComponent } from './Shared/SideBar/SideBar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//
@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SideBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ProductModule,
    SlimLoadingBarModule,
    MDBBootstrapModule,
    FormsModule,
    ReactiveFormsModule,
    MoviesModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
