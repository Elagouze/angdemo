import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-account-detail',
  templateUrl: './account-detail.component.html',
  styleUrls: ['./account-detail.component.css']
})
export class AccountDetailComponent implements OnInit {

  accountForm: FormGroup;
  constructor(private fb: FormBuilder) {
    this.accountForm = this.fb.group({

    });
   }

  ngOnInit() {
  }

}
