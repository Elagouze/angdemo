import { AccountDetailComponent } from './account-detail/account-detail.component';
import { UserDetailComponent } from './User-Detail/User-Detail.component';
import { FormDemoComponent } from './FormDemo/FormDemo.component';
import { ProductFormComponent } from './ProductForm/ProductForm.component';
import { ProductRoutingModule } from './Product-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductListComponent } from './ProductList/ProductList.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
     ProductRoutingModule,
     FormsModule,
     ReactiveFormsModule
  ],
  declarations: [
    ProductFormComponent,
    ProductListComponent,
    FormDemoComponent,
    UserDetailComponent,
    AccountDetailComponent
  ]
})
export class ProductModule { }
