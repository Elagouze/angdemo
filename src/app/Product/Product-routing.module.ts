import { FormDemoComponent } from './FormDemo/FormDemo.component';
import { ProductListComponent } from './ProductList/ProductList.component';
import { ProductFormComponent } from './ProductForm/ProductForm.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';


const routes: Routes = [
    {
        path: 'Create',
        component: ProductFormComponent
    },
    {
        path: 'Edit/:id',
        component: ProductFormComponent
    },
    {
        path: '',
        component: ProductListComponent
    },
    {
        path: 'FormDemo',
        component: FormDemoComponent
    },
    {
        path: '**',
        redirectTo: '',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ProductRoutingModule { }
