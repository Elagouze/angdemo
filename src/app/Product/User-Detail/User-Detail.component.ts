import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-User-Detail',
  templateUrl: './User-Detail.component.html',
  styleUrls: ['./User-Detail.component.css']
})
export class UserDetailComponent implements OnInit {
  userDetailForm: FormGroup;
  genders: ['Male', 'Female'];
  // tslint:disable-next-line: variable-name
  country_phone_group: ['EGY', 'UAE'];
  constructor(private fb: FormBuilder) {
    this.userDetailForm = this.fb.group({
      fullname: ['Homero Simpson', Validators.required ],
      // tslint:disable-next-line: max-line-length
      bio: ['Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', Validators.maxLength(256)],
      birthday: ['', Validators.required],
     // gender: new FormControl(this.genders[0], Validators.required),
      country_phone: this.country_phone_group
    });
   }

  ngOnInit() {
  }

}
