import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-ProductForm',
  templateUrl: './ProductForm.component.html',
  styleUrls: ['./ProductForm.component.css']
})
export class ProductFormComponent implements OnInit {
  model: FormGroup;
  constructor(private fb: FormBuilder) {
    this.createForm();
   }
  ngOnInit() {
  }

  private createForm() {
    this.model = this.fb.group({
      Name: ['', Validators.required],
      Description : ['', Validators.required]
    });
  }
}
