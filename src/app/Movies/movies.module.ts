import { MovieRoutingModule, MoviesComponet } from './movies-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    MovieRoutingModule
  ],
  declarations: [MoviesComponet ]
})
export class MoviesModule { }
