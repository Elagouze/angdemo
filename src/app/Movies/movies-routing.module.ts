import { MoviesListComponent } from './movies-list/movies-list.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule, Component } from '@angular/core';
import { MovieFormComponent } from './movie-form/movie-form.component';


const routes: Routes = [
      {
         path: '',
         component: MoviesListComponent
      },
      {
          path: 'Create',
          component: MovieFormComponent
      },
      {
          path: 'edit:/id',
          component: MovieFormComponent
      }
];
@NgModule({
    imports : [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class MovieRoutingModule { }
export const MoviesComponet = [MoviesListComponent, MovieFormComponent];
